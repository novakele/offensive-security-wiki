# Windows

## Enable Sysmon

- [sysmon-config](https://github.com/Neo23x0/sysmon-config)
Run as admin: `sysmon.exe -accepteula -c sysmonconfig-export-block.xml`

## Disable Legacy DNS
* Disable mDNS
```powershell
New-Item "HKLM:\SOFTWARE\Policies\Microsoft\Windows NT" -Name DNSClient -ErrorAction SilentlyContinue 
New-ItemProperty "HKLM:\SOFTWARE\Policies\Microsoft\Windows NT\DNSClient" -Name EnableMultiCast -Value 0 -PropertyType DWORD -Force
```
* Disable LLMNR
```powershell
New-Item "HKLM:\SOFTWARE\Policies\Microsoft\Windows NT" -Name DNSClient -ErrorAction SilentlyContinue
New-ItemProperty "HKLM:\SOFTWARE\Policies\Microsoft\Windows NT\DNSClient" -Name DisableSmartNameResolution -Value 0 -PropertyType DWORD -Force
```
* Disable NetBIOS on all network interfaces
```powershell
$regkey = "HKLM:SYSTEM\CurrentControlSet\services\NetBT\Parameters\Interfaces"
Get-ChildItem $regkey `
    | ForEach-Object {Set-ItemProperty -Path "$regkey\$($_.pschildname)" -Name NetbiosOptions -Value 2 -Verbose}
```
## Disable WPAD (Proxy Discovery)
```powershell
New-Item "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\" -Name WinHttp -ErrorAction SilentlyContinue
New-ItemProperty "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\WinHttp\" -Name "DisableWpad" -Value 4 -PropertyType DWORD -Force
```

## Disable IPv6 on all interfaces
```powershell
Get-NetAdapterBinding `
    | Where-Object {$_.ComponentID -EQ 'ms_tcpip6' -and $_.Enabled -EQ 'False'} `
    | ForEach-Object {Disable-NetAdapterBinding -Name $_.Name -ComponentID 'ms_tcpip6' -ErrorAction SilentlyContinue -Verbose; Start-Sleep -Seconds 3}
```

## Enable Virtualization Based Security
```powershell
HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\DeviceGuard
New-Item "HKLM:\SYSTEM\CurrentControlSet\Control\" -Name DeviceGuard -ErrorAction SilentlyContinue
New-ItemProperty "HKLM:\SYSTEM\CurrentControlSet\Control\DeviceGuard" -Name "EnableVirtualizationBasedSecurity" -PropertyType DWORD -Value 1 -Force
```

## Enable Windows Defender Credential Guard
```powershell
New-Item "HKLM:\SYSTEM\CurrentControlSet\Control\" -Name Lsa -ErrorAction SilentlyContinue
New-ItemProperty "HKLM:\SYSTEM\CurrentControlSet\Control\Lsa" -Name "LsaCfgFlags" -PropertyType DWORD -Value 2 -Force
```