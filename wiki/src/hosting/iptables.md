# iptables

## DIY portquiz.net

```shell
# /etc/iptables/rules.v4
*nat
:PREROUTING ACCEPT [0:0]
:POSTROUTING ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
-A PREROUTING -i lo -j RETURN
-A PREROUTING -p icmp -j RETURN
-A PREROUTING -m state --state RELATED,ESTABLISHED -j RETURN
-A PREROUTING -p tcp -j DNAT --to-destination :8888
COMMIT

*filter
:INPUT ACCEPT [0:0]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
-A INPUT -p icmp -j ACCEPT
-A INPUT -i lo -j ACCEPT
-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
-A INPUT -p tcp -m state --state NEW -m tcp --dport 8888 -j ACCEPT
-A INPUT -j DROP
COMMIT
```

```shell
# /etc/xinetd.d/alive
service simple-tcp
{
    disable         = no
    type            = UNLISTED
    id              = tcp-service
    socket_type     = stream
    port            = 8888
    protocol        = tcp
    user            = nobody
    wait            = no
    server          = /bin/echo
    server_args     = Alive!
    instances       = 25
    log_on_success  += DURATION HOST
    log_on_failure  += ATTEMPT HOST
    per_source      = 5
    flags           = IPv4
}
EOF
```