# wfuzz
- look for ssrf
wfuzz -z range,0-10000 -u http://dc01.heist.offsec:8080/?url=http://localhost:FUZZ --field url --hh 178435


---
dirbuster
wfuzz -z file,/opt/SecLists/Discovery/Web-Content/raft-small-directories.txt -u http://$IP:10000/FUZZ --hh 12



---
fuzz json api + proxy
wfuzz -p localhost:8080:HTTP -c -z file,/opt/SecLists/Usernames/top-usernames-shortlist.txt -H 'Content-Type: application/json' -d '{"user":"FUZZ","url":"http://192.168.49.113"}' -u http://$IP:13337/update