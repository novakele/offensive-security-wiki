# smbclient
- list shares 
smbclient -L '//192.168.240.152'

- list shares with null session
smbclient -L '//192.168.240.152' -U ' ' -p ''

- Download all files from share recursively without prompt
smbclient //10.11.1.31/wwwroot
smb: \> prompt
smb: \> mask ""
smb: \> recurse ON
smb: \> mget *

---


smbclient //$IP/backup -U 'spookysec.local/svc-admin' --password management2005
