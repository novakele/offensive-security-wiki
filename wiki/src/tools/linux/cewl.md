# cewl

* Fetch a list of all pages with content (either manually or with feroxbuster)
Save all words with lenght >= 5 chars -> run bruteforce with hydra
```shell
for url in $(cat urls.txt); do echo $url && cewl -d 5 $url >> temp_cewl.txt;done
cat temp_cewl.txt | sort -u >> cewl.txt && rm temp_cewl.txt
```
---
`cewl --depth 5 --min_word_length 5 --with-numbers --write cewl.log `
