# autorecon

- [project repository](https://github.com/Tib3rius/AutoRecon)

Examples:

```shell 
autorecon $TARGET --global.domain $TARGET_DOMAIN.TLD --single-target
```