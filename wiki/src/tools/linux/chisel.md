# chisel
* To forward port from remote host to localhost kali

* On kali
`chisel_linux sever --port 4434 --reverse`
`./chisel_1.7.7_linux_amd64 server --port 4434 --reverse`

- on remote host
`chisel_windows client <kali ip>:<kali port> R<kali host ip>:<kali host port>:<remote host ip>:<remote host port>`
`chisel_windows client 192.168.88.128 R10.10.14.210:4444:127.0.0.1:4444`