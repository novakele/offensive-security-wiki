# gobuster
* prepend command with HTTP_PROXY="socks5://127.0.0.1:1337" gobuster...

`HTTP_PROXY="socks5://127.0.0.1:1337" gobuster dir -u http://172.16.1.13:80 -w /opt/SecLists/Discovery/Web-Content/raft-small-words.txt -x html,php,txt,zip,bak | tee recon/port80-raft-words-phphtmltxtzipbak`


* custom useragent
`-a 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36'`