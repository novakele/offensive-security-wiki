# nmap
* Interactive nse script lookup
https://www.infosecmatter.com/nmap-nse-library/


* Get tops N UDP ports -- for udp scan behind proxy 
sort -r -k3 /usr/share/nmap/nmap-services \
| grep '/udp' \
| head -n 100 \
| cut -f 2 \
| cut -d'/' -f 1 \
| xargs -I{} echo -n "{},"