# certbox

```shell
certbot certonly --webroot --preferred-challenges http -d <domain_fqdn> --webroot-path <webroot_path> --agree-tos -m <contact_email_address>
```
