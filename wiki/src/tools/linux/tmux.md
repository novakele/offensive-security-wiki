# tmux
- Shared tmux session
tmux new-session -s <session name>
tmux attach-session -t <session name>



- Shared tmux session with another user
tmux -S /tmp/<socket name> new -s <session name> # Create session
chgrp <group name> /tmp/<socket name> # change group of socket (other user needs that group)
tmux -S /tmp/<socket name> attach -t <session name> # from the other user, attach to the session using the socket