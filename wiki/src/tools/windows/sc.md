# sc
- Change binary_path_name of service
`cmd /c "sc config IObitUnSvr binPath= c:\Users\dharding\IObit.exe"`

- Start service
`cmd /c "sc start IObitUnSvr"`

- Stop service
`cmd /c "sc stop IObitUnSvr"`

- Query service
`cmd /c "sc qc IObitUnSvr"`