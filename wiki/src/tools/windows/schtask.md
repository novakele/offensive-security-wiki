# schtask
- list scheduled tasks
`schtasks /query /fo LIST /v`

- list scheduled tasks + grep
`schtasks /query /fo LIST /v | select-string 'TFTP' -context 10 `