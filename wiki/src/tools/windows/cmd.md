# cmd

- c:\windows\system32\cmd.exe
- C:\Windows\sysWOW64\cmd.exe
- C:\Windows\sysNative\cmd.exe

https://ss64.com/nt/syntax-64bit.html


|  | 32 bit folder | 64 bit folder |
|:----:|:----:|:----:|
| 32 bit session | C:\Windows\system32\ | C:\Windows\sysNative\ |
| 64 bit session | C:\Windows\sysWOW64\ | C:\Windows\system32\ |

