# net
* https://en.wikiversity.org/wiki/Net_(command)/Localgroup

- mount share as letter with <username:bobby> <password: bobby> # /USER is case sensitive
`net use g: \\10.10.14.235\tools /USER:bobby bobby`

- list all services
`net start`

- create user
`net user username password /all`

- add user to local admin group
`net localgroup Administrators ariah /add`