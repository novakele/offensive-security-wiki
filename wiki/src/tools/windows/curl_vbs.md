# curl vbs

```shell
echo 'Set colNamedArguments = WScript.Arguments.Named' > curl.vbs
echo 'url = colNamedArguments.Item("url")' >> curl.vbs
echo 'data = ""' >> curl.vbs
echo 'ContentType = "application/x-www-form-urlencoded"' >> curl.vbs
echo 'If colNamedArguments.Exists("method") Then' >> curl.vbs
echo '  method = colNamedArguments.Item("method")' >> curl.vbs
echo 'Else' >> curl.vbs
echo '  method = "GET"' >> curl.vbs
echo 'End If' >> curl.vbs
echo 'If colNamedArguments.Exists("json") Then' >> curl.vbs
echo '    ContentType = "application/json"' >> curl.vbs
echo 'End If' >> curl.vbs
echo 'If colNamedArguments.Exists("dataFile") Then' >> curl.vbs
echo '  Set objFS = CreateObject("Scripting.FileSystemObject")' >> curl.vbs
echo '  dataFile = colNamedArguments.Item("dataFile")' >> curl.vbs
echo '  Set dataFileHandle = objFS.OpenTextFile(dataFile)' >> curl.vbs
echo '  Do Until dataFileHandle.AtEndOfStream' >> curl.vbs
echo '    data = data & dataFileHandle.ReadLine' >> curl.vbs
echo '  Loop' >> curl.vbs
echo '  dataFileHandle.Close' >> curl.vbs
echo 'End If' >> curl.vbs
echo 'dim xmlhttp: Set xmlhttp = CreateObject("MSXML2.ServerXMLHTTP")' >> curl.vbs
echo 'xmlhttp.Open method, url, False' >> curl.vbs
echo 'xmlhttp.setRequestHeader "Content-Type", ContentType' >> curl.vbs
echo 'xmlhttp.send data' >> curl.vbs
echo 'WScript.Echo xmlhttp.responseText' >> curl.vbs
echo 'Set xmlhttp = Nothing' >> curl.vbs
```
---
* Usage
' GET request
`cscript \\samba\share\scripts\curl.vbs /url:http://example.com/`

' POST request with URL encoded POST data in a file
`cscript \\samba\share\scripts\curl.vbs /url:http://example.com/ /dataFile:urlEncodedData.txt /method:POST`

' POST request with JSON POST data in a file
`cscript \\samba\share\scripts\curl.vbs /url:http://example.com/ /dataFile:jsonData.txt /method:PATCH /json`


* https://gist.github.com/cabloo/387f1d6d8fe81752574d