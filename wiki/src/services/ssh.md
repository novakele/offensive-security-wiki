# SSH

- bitvise ssh server for windows
```
Terminal initialization failure. See server logs for more info.
Hint: Try requesting a different terminal environment.
Connection to 192.168.206.179 closed.
```
Solution: ssh -i ~/.ssh/dvr4_viewer_key viewer@$IP "cmd"


### Proxy
#### Forward Socks Proxy
`ssh -D <port>`
#### Reverse Socks Proxy
`ssh -R <port>`

### Port Forwarding
#### Local Port Forwarding

#### Remote Port Forwarding