# Shares

### FTP
- try anonymous
- try login with /opt/SecLists/Passwords/Default-Credentials/ftp-betterdefaultpasslist.txt

---
ftp -A $IP # use active ftp
---


---
* recursive download (do not create host directory -nH)
wget -nH -r ftp://anonymous:anonymous@$IP:30021


--- 
* recursive download active FTP
wget -nH -r --no-passive-ftp ftp://anonymous:anonymous@$IP

### GlusterFS
https://book.hacktricks.xyz/network-services-pentesting/24007-24008-24009-49152-pentesting-glusterfs

sudo gluster --remote-host=10.10.11.131 volume list
* This will return the name of the volumes

sudo mount -t glusterfs 10.10.11.131:/<vol_name> /mnt/



* Sirol glusterfs privesc
https://github.com/MauroEldritch/GEVAUDAN/blob/master/gevaudan.rb


### NFS
* Mount NFS share with proxy
* `-L local_ip:local_port:remote_ip:remote_port`

`ssh -L 127.0.0.1:2049:127.0.0.1:2049`
`sudo mount -t nfs -o port=2049,proto=tcp localhost:/srv/Share nfs_mnt/`


`sudo proxychains -q nmap -sT -p111,2049 --script 'nfs*' -oN recon/nfs-service 172.16.1.5 -vv`

---

- Check if no_root_squash -> permissions (uid:gid) are checked client side


### SMB
* ERRORS (protocol negotiation failed:)
add those two line in */etc/samba/smb.conf* under [global]

client min protocol = LANMAN1
client max protocol = SMB3