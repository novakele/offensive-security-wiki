# Mail

## SMTP
* Enumerate users
smtp-user-enum -M VRFY -U users.txt -t $IP

* brute force pop3 login (hydra is unstable)
medusa -U users.txt -P cewl_passwords -M pop3 -h $IP


* brute force imap login
hydra -L smtp_users.txt -P cewl_passwords imap://$IP -I

---
| Command   | Comment                                                                                          |
|:-----------:|:----------------------------------------------------------------------------------------------|
| ATRN      | Authenticated TURN                                                                               |
| AUTH      | Authentication                                                                                   |
| BDAT      | Binary data                                                                                      |
| BURL      | Remote  content                                                                                  |
| DATA      | The actual email message to be sent. This command is terminated with a line that contains only a |
| EHLO      | Extended HELO                                                                                    |
| ETRN      | Extended turn                                                                                    |
| EXPN      | Expand                                                                                           |
| HELO      | Identify yourself to the SMTP server.                                                            |
| HELP      | Show available commands                                                                          |
| MAIL      | Send mail from email account                                                                     |
| MAIL      | FROM: me@mydomain.com                                                                            |
| NOOP      | No-op. Keeps you connection open.                                                                |
| ONEX      | One message transaction  only                                                                    |
| QUIT      | End session                                                                                      |
| RCPT      | Send email to recipient                                                                          |
| RCPT      | TO:you@yourdomain.com                                                                            |
| RSET      | Reset                                                                                            |
| SAML      | Send and mail                                                                                    |
| SEND      | Send                                                                                             |
| SOML      | Send or mail                                                                                     |
| STARTTLS  |                                                                                                  |
| SUBMITTER | SMTP responsible submitter                                                                       |
| TURN      | Turn                                                                                             |
| VERB      | Verbose                                                                                          |
| VRFY      | Verify                                                                                           |
---


## POP
--- pop3 server with netcat
| Command       | Comment                                       |
|---------------+-----------------------------------------------|
| USER          | Your user name for this mail server           |
| PASS          | Your password.                                |
| QUIT          | End your session.                             |
| STAT          | Number and total size of all messages         |
| LIST          | Message# and size of message                  |
| RETR message# | Retrieve selected message                     |
| DELE message# | Delete selected message                       |
| NOOP          | No-op. Keeps you connection open.             |
| RSET          | Reset the mailbox. Undelete deleted messages. |




Enumerate valide users with known password + mail (check oscp beta)
#!/bin/bash

pass=1234

for user in marcus john mailadmin jenny ryuu
do
        (echo USER $user; sleep 2s; echo PASS $pass; sleep 2s; echo LIST; sleep 2s; echo QUIT)| ncat -nvC 10.11.1.72 110
done

medusa -U users.txt -P cewl_passwords -M pop3 -h $IP



---

telnet $IP 110

user <username>
pass <password>
list # show email list

retr <email id> # show content of email
## IMAP
