# Enumeration
### API
* check all http methods on each endpoints

### Listening ports
* `netstat -ano | findstr /i /c:"listen"`
* `netstat -tulpn`

`WMIC path win32_process get Caption,Processid,Commandline`


## Windows
* list drives
`wmic logicaldisk get caption,description,providername`

* list installed patches
`wmic qfe list full /format:list`

* list installed applications
`wmic product get name,version`
`wmic product get name,version,installlocation`

* list services
`net start`
`wmic service get DisplayName,name,startname`
`wmic service get DisplayName,name,startname,pathname`
`wmic service get name,displayname,pathname,startmode |findstr /i "auto"`

* query config of service
`sc qc upnphost`

* update service binary path
`sc config upnphost binpath= "C:\Inetpub\Scripts\rv.exe"`
* change user running service
`sc config upnphost obj= ".\LocalSystem" password= ""`


*
`cmd.exe /c sc queryex state=all type=service`
`Get-Service | findstr -i "manual"`
`gwmi -class Win32_Service -Property Name, DisplayName, PathName, StartMode | Where {$_.PathName -notlike "C:\Windows*" -and $_.PathName -notlike '"*'} | select PathName,DisplayName,Name`
`gwmi -class Win32_Service -Property Name, DisplayName, PathName, StartMode | Where {$_.StartMode -eq "manual"} | select PathName,DisplayName,Name`