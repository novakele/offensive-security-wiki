# Reverse Shell
* Should outgoing ports be filtered, try ports that are in use by the machine
For exemple:
Starting Nmap 7.92 ( https://nmap.org ) at 2022-06-13 21:01 EDT
Nmap scan report for 192.168.54.64
Host is up (0.049s latency).
Not shown: 65529 filtered tcp ports (no-response)
PORT     STATE SERVICE
21/tcp   open  ftp
22/tcp   open  ssh
139/tcp  open  netbios-ssn
445/tcp  open  microsoft-ds
3306/tcp open  mysql
8003/tcp open  mcreport


use port 21,22,139,445,3306,8003
---