# Raspberry Pi

### Builtin power on/off button
- Connect a button to pin 5 and ground to issue a soft shutdown.
```ini
[pi4]
dtparam=i2c_arm=off
dtoverlay=gpio-shutdown,gpio_pin=3,active_low=1
```